This repo has the constraint that everything here is implemented from scratch, except the basic computational procedures, like efficient linear algebra, etc.

# Deep net.
The source code can be found in architectures/nn.py. 
Uses minibatch stochastic gradient descent during back propagation to update weight values.
A theoretical explanation of backpropagation can be found in theory/backpropagation.tex.
# Boltzmann Machine 
This is yet to be implemented.

requires python3.10
