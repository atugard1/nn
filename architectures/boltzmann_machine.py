
class Boltzmann_Machine:
    def __init__(self, units, threshold, weights=[]):
        self.units = np.array(units)
        if weights == []:
            weights = np.array([[np.random.uniform(-1,1) for x in range(len(units))] for x in range (len(units))])
            weights = (weights + weights.T)/2
        bias = np.zeros(len(weights))
        for i in range(len(weights)):
            bias[i] = abs(weights[i][i])
            weights[i][i] = 0
        self.weights = weights
        self.threshold = threshold
        self.bias = np.array(bias)
        self.dim = len(units)
        
    def energy(self):
        return -np.dot(self.units, np.dot(self.weights, self.units)) -np.dot(self.bias, self.units)
    
    def run(self):
        p = random_permutation(self.dim)
        for i in p:
            if np.dot(self.weights[i], self.units) + self.bias[i] >= self.threshold[i]:
                self.units[i] = 1
            else:
                self.units[i] = -1
        return self.units

    def train(self,xs_train, learning_rate):
        for x in xs_train:
            x = np.array(x)
            weight_increment = x.reshape(self.dim,1) * x
            for i in range(self.dim):
                weight_increment[i][i] = 0
            self.weights += learning_rate * weight_increment
            self.bias += learning_rate * x
