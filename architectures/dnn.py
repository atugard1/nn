import numpy as np
import random as rand
from common_libs.linear import make_matrices
from common_libs.calculus import total_derivative
from common_libs.permutations import permute_list, random_permutation
from common_libs.regularization_fxns import l2_regularization
#import aesara
#from aesara import tensor as at

#to do: add regularization, i.e. change loss fxns.
#       audoencoders
#       adjust implementation as to allow convolutional neural networks, i.e. add convolution and pooling layer functionality and generalize backprop so as to deal with these cases.
#       rewrite using aesara



############################################# NEURAL NET IMPLEMENTATION #############################################


class Layer:
    def __init__(self, vals, fxn):
        self.vals = vals
        self.fxn = fxn
    def copy(self):
        return Layer(self.vals, self.fxn)
    

class DNN:
    def __init__(self, dims:list[int], afxns, layers:list[Layer] = [], weights=[]):
        if layers:
            self.layers = layers
        else:
            self.layers = []
            for (i,dim) in zip(range(len(dims)), dims):
                self.layers += [Layer([0]*dim, afxns[i])]
        if weights:
            self.weights = weights
        else:
            self.weights = make_matrices(dims)
            
        self.num_layers = len(self.layers)
    def set_weight_matrix(self, i, weight_matrix):
        """
        (1st arg) i             : index of weight matrix
        (2nd arg) weight_matrix : new weight_matrix
        Sets weight_matrix at index i to weight_matrix
vv        """
        self.weights[i] = weight_matrix

    def layer(self,n):
        """
        (1st arg) n: layer number
        returns layer number n
        """
        return self.layers[n]
    def set_fxn(self, n, fxn):
        """
        (1st arg) n : layer number
        (2nd arg) f : a fxn that maps the # of units in layer n to the number of units in the layer n + 1
        replaces the fxn at layer n with f.
        """
        self.layer(n).fxn = fxn
    
    def vals(self, n):
        """
        takes layer number as argument.
        """
        return np.array(self.layer(n).vals)
    def fxn(self, n):
        """
        takes layer number as argument.
        """
        return self.layer(n).fxn

    def data(self,n):
        """
        returns layer values and activation functions at nth layer.
        """
        return (self.vals(n), self.fxn(n))

    def apply_fxn(self, n):
        """
        applies unit activation fxns to unit values across the whole layer, and returns the result.
        """
        vals = []
        return self.fxn(n)(self.vals(n))
    def inputs(self):
        """
        returns current net input."
        """
        return np.array(self.vals(0))
    def outputs(self):
        """
        returns current net output.
        """
        return np.array(self.apply_fxn(self.num_layers-1))
    def compose(self, nn, weight_param, learning_rate, weight_matrix = [], contract=0):
        """
        composes the network with another network (first argument), and returns the result.
        (optional) second argument specifies a weight_matrix to use for the composition,
        (optional) third argument specifies whether in the case of same dimensional output layers and input layers the two should be contracted to one layer.
        """
        layers = []        
        if contract == 1 and (len(self.outputs()) == len(nn.inputs())):
            for layer in self.layers[:-1]:
                layers += [layer.copy()]
            for layer in nn.layers:
                layers += [layer.copy()]
            weights = self.weights.copy() + nn.weights.copy()
        else:
            for layer in self.layers:
                layers += [layer.copy()]
            for layer in nn.layers:
                layers += [layer.copy()]
            weights = self.weights.copy() + nn.weights.copy()            
            if weight_matrix == []:
                weight_matrix = np.random.rand(len(self.outputs()), len(nn.inputs()))
            else:
                weights = self.weights.copy() + [weight_matrix] + nn.weights.copy()
        return NN([],[],weight_param, learning_rate, layers, weights)
    def decompose(self, n):
        """
        returns two nets, one from the inputs to the nth layer and another from the nth layer to the outputs.
        """
        if n==0 or n==self.num_layers:
            return self
        
        layers1 = []
        for layer in self.layers[0:n]:
            layers1 += [layer.copy()]
        layers2 = []
        for layer in self.layers[n:self.num_layers]:
            layers2 += [layer.copy()]            
            
        weights1 = self.weights[0:n].copy()
        weights2 = self.weights[n:self.num_layers-1].copy()
        
        return [NN([],[], self.learning_rate, self.weight_param, layers1, weights1), NN([],[], self.learning_rate, self.weight_param, layers2, weights2)]
    
    def invert(self):
        """
        inverts the network by reversing the arrows.
        """
        layers = []
        for layer in reversed(self.layers):
            layers  += [layer.copy()]
        
        weights = list(reversed(self.weights.copy()))
        for i in range(len(weights)):
            weights[i] = weights[i].T
        return NN([],[], layers, weights)    
    
    def update_vals(self, n, values):
        """
        updates the values of the nth layer (first argument) with values (second argument)
        """
        self.layer(n).vals = values
            
    def load_inputs(self, values):
        """
        updates the value of the 0th layer
        """
        self.update_vals(0, values)
        
    def propagate(self, n):
        """
        applies activation function to the values at that layer, then propagates weighted values to next layer.        
        argument n is the layer to propagate.

        """
        self.update_vals(n+1, np.dot(self.weights[n].T, self.apply_fxn(n)))
            
    def run(self,inputs=[]):
        """
        loads the inputs provided as argument, and propagatess the network, returning the output value
        """
        if inputs == []:
            inputs = np.random.rand(len(self.inputs()), 1)
        self.load_inputs(inputs)
        for k in range(self.num_layers-1):
            self.propagate(k)
        return self.outputs()
    #this can probably be made way cleaner in a language like haskell...
    def backprop(self, batch_xs, batch_ys, loss_fxn, learning_rate, weight_param):
        """
        (1st arg) batch_xs   : batch of training inputs
        (2st arg) batch_ys   : batch of training outputs
        (3rd arg) loss_fxn: loss function
        Runs the network on the batch inputs, keeps track of all network values, and runs backpropagation on these to update the weights, with loss computed based on training outputs.
        """
        n = self.num_layers -1        
        batch_size = len(batch_xs)
        vals = []

        total_loss = 0        
        for i in range(batch_size):
            #run batch_xs[i] through the network, and compute the loss.
            total_loss += loss_fxn(batch_ys[i])(self.run(batch_xs[i]))
            temp = []
            for j in range(self.num_layers):
                temp += [self.vals(j)]
            vals += [temp]
        print("total loss:", total_loss)
        total_loss = 0
            
        deltas = []
        for batch_index in range(batch_size):
            delta = total_derivative(loss_fxn(batch_ys[batch_index]), self.fxn(n)(vals[batch_index][n]))
            dydx = total_derivative(self.fxn(n), vals[batch_index][n])
            deltas += [np.dot(dydx.T, delta)]
        
        (num_rows,num_columns) = self.weights[n-1].shape
        for batch_index in range(batch_size):
            y = self.fxn(n-1)(vals[batch_index][n-1])
            delta = deltas[batch_index]
            delta = delta.reshape(1,len(delta))
            self.weights[n-1] -= learning_rate * (y.reshape(len(y),1) * delta + weight_param * regularization_fxn(self.weights[n-1-k]))

        for k in range(1, n):
            (num_rows,num_columns) = self.weights[n-1-k].shape
            for batch_index in range(batch_size):
                dydx = total_derivative(self.fxn(n-k), vals[batch_index][n-k])                
                deltas[batch_index] = np.dot(np.dot(dydx.T, self.weights[n-k]), deltas[batch_index])
                y = self.fxn(n-1-k)(vals[batch_index][n-1-k])
                delta = deltas[batch_index]
                delta = delta.reshape(1,len(delta))
                self.weights[n-1-k] -= learning_rate * (y.reshape(len(y),1) * delta + weight_param * regularization_fxn(self.weights[n-1-k]))

    def train_once(self, inputs, outputs, batch_size, loss_fxn, learning_rate, weight_param):
        """
        (1st arg) inputs        : list or array of inputs
        (2nd arg) outputs       : list or array of outputs
        (3rd arg) batch_size    : size of batches                
        (4th arg) loss_fxn      : a loss function
        (5th arg) learning rate : learning rate
        (6th arg) weight_param  : regularization weight parameter                
        Trains the network, 
        """        

        p = random_permutation(len(inputs))
        inputs = permute_list(inputs, p)[:batch_size]
        outputs = permute_list(outputs, p)[:batch_size]

        self.backprop(inputs[:batch_size], outputs[:batch_size], loss_fxn, learning_rate, weight_param)



    def train(self, epochs, trials, inputs, outputs, batch_size, loss_fxn, learning_rate, weight_param):
        """
        (1st arg) epochs        : number of epochs
        (2nd arg) trials        : number of trials
        (3rd arg) inputs        : list or array of inputs
        (4th arg) outputs       : list or array of outputs
        (5th arg) batch_size    : size of batches                
        (6th arg) loss_fxn      : a loss function
        (7th arg) learning rate : learning rate
        (8th arg) weight_param  : regularization weight parameter                
        Trains the network, 
        """
        for epoch in range(epochs):
            print("epoch", epoch)
            print("----------------------")
            for trial in range(trials):
                print("trial:", trial+1, "/", trials)
                self.train_once(inputs, outputs, batch_size, loss_fxn, learning_rate, weight_param)


#preliminary simple implementation of these two.
#this is just a fxn based on matrix operations. you can compute on paper what the derivative ought to be, and make the appropriate modifications to compute it.
#the derivatives in the convolution layers might end up being faster than the usual matrix multiplication.
def convolution():
    return None
#not sure what the gradients here will be... probably simple. who knows.
def max_pooling():
    return None
