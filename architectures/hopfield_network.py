import random
import numpy as np
from common_libs.permutations import random_permutation

kronecker_delta = lambda x : lambda y: 1 if x == y else 0

class Hopfield_Network:
    def __init__(self, states, threshold, weights=[]):
        self.states = np.array(states)
        if weights == []:
            weights = np.array([[np.random.uniform(-1,1) for x in range(len(states))] for x in range (len(states))])
            weights = (weights + weights.T)/2
        bias = np.zeros(len(weights))
        for i in range(len(weights)):
            bias[i] = abs(weights[i][i])
            weights[i][i] = 0
        self.weights = weights
        self.threshold = threshold
        self.bias = np.array(bias)
        self.dim = len(states)
    def set_states(self,states):
        self.states = states
        
        
    def energy(self):
        return -np.dot(self.states, np.dot(self.weights, self.states)) -np.dot(self.bias, self.states)
    
    def run(self):
        n = random.randint(0,self.dim-1)
        if np.dot(self.weights[n], self.states) + self.bias[n] >= self.threshold[n]:
            self.states[n] = 1
        else:
            self.states[n] = -1
        return self.states

    def train(self,xs_train, learning_rate):
        for x in xs_train:
            x = np.array(x)
            weight_increment = x.reshape(self.dim,1) * x
            for i in range(self.dim):
                weight_increment[i][i] = 0
            self.weights += learning_rate * weight_increment
            self.bias += learning_rate * x


        
            
            
            
            
        
        
        
        
