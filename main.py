import numpy as np
from architectures.dnn import DNN
from common_libs.activation_fxns import identity, sigmoid, relu, relu_clipped, relu_scaled, tanh, tanh_clipped, hard_tanh, softmax
from common_libs.loss_functions import l1_loss, l2_loss, hinge1_loss, hinge2_loss, cross_entropy_loss
from common_libs.mathematics import argmax
from tensorflow.keras import datasets 

datasets.mnist.load_data(path="mnist.npz")

#data preprocessing
(x_train, y_train), (x_test, y_test) = datasets.mnist.load_data()
x_train = [(x/255).flatten() for x in x_train]
#convert y_train output labels to one hot coded format.
y_train = [[y] for y in y_train]
temp = [np.array([0]*10) for y in y_train]
for (y,t) in zip(y_train,temp):
   t[y]=1
y_train = temp

#define neural net architecture
dnn = DNN([784, 128, 10], [identity, relu_scaled(0.01), softmax])

#simple training function. all arguments are given by default, so this could be run simply as t()
def t(epochs=49, learning_rate = 0.001, weight_param = 0, batch_size = 128, loss_fxn = cross_entropy_loss):
    return dnn.train(epochs, 784, x_train, y_train, batch_size, loss_fxn, learning_rate, weight_param)
