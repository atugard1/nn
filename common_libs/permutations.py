import random as rand
def random_permutation(n):
    """
    (1st arg): a positive integer n
    Returns a random permutation of [1,2,...,n]
    """
    p = [k for k in range(n)]
    rand.shuffle(p)
    return p

def permute_list(xs,p):
    """
    (1st arg) xs: a list
    (2nd arg) p: a permutation
    Returns list xs permuted according to p.
    """
    n = len(xs)
    new_xs = [0]*n
    for (i,j) in zip(range(n), p):
        new_xs[i] = xs[j]
    return new_xs
        

