def argmax(f, args):
    x = args[0]
    max_val = f(*x)
    max_arg = x
    for arg in args:
        if f(*arg) > max_val:
            max_val = f(*arg)
            max_arg = arg
    return max_arg

def argmin(f, args):
    x = args[0]
    min_val = f(*x)
    min_arg = x
    for arg in args:
        if f(*arg) < min_val:
            min_val = f(*arg)
            min_arg = arg
    return min_arg 
