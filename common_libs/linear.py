import numpy as np

def delta(x,y):
    """
    (1st arg) x: a value comparable with y
    (2nd arg) y: a value comparable with x
    Returns 1 if the two values are the same, and zero otherwise. In the case that x and y are floating point numbers, this is the discrete dirac delta function.
    """
    return 1 if x == y else 0

def is_diag(mat):
    """
    (1st arg) mat: matrix.
    Returns True if the matrix is diagonal, and false otherwise
    """
    for i in range(len(mat)):
        for j in filter(lambda k: k!=i, range(len(mat))):
            if mat[i][j] != 0:
                return False
    return True

def make_matrices(dims):
    result = []
    for i in range(len(dims)-1):
        result += [np.random.rand(dims[i], dims[i+1])]
    return result


