import math
import numpy as np
#activation functions

#can this be generalized to something that can create pooling, 
def layer_fxn(f):
    """
    (1st arg) f: function taking argument of type x
    (2nd arg) x: argument
    if x is a number, returns f(x), if x is an array returns np.array([f(x_1), f(x_2), ..., f(x_n)]) 
    """
    def fxn(x):
        if type(x) == float or type(x) == int:
            return f(x)
        elif type(x) == np.ndarray or type(x) == list:
            return np.array(list(map(f, x)))
        else:
            raise Exception("Argument must be either a float, numpy.ndarray, or list. You gave:" + str(type(x)))
    return fxn

identity = layer_fxn(lambda x: x)
sigmoid = layer_fxn(lambda x: 1/(1+math.exp(-x)))
relu = layer_fxn(lambda x: max(0,x))
relu_clipped =  layer_fxn(lambda x: min(max(0,x),1))
relu_scaled = (lambda c: layer_fxn(lambda x: max(0,c*x)))
tanh = layer_fxn(lambda x: (math.exp(2*x)-1)/(math.exp(2*x)+1))
tanh_clipped = layer_fxn(lambda x: min((math.exp(2*x)-1)/(math.exp(2*x)+1), 1))
hard_tanh = layer_fxn(lambda x: max(min(x,1), -1))
def softmax(xs):
    result = []
    for x in xs:
        result += [math.exp(x)]
    return 1/(sum(map(lambda x: math.exp(x), xs))) * np.array(result)




