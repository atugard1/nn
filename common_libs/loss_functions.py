import numpy as np
import math

#regularization parameters are introduced

def l1_loss(real_out):
    return (lambda ys: math.sqrt(np.dot(ys-real_out, ys-real_out)))
def l2_loss(real_out):
    return (lambda ys: np.dot(ys-real_out, ys-real_out)) 
def hinge1_loss(real_out):
    return (lambda ys: sum(map(lambda pair: max(0,1-pair[0]*pair[1]),  zip(ys,real_out))))
def hinge2_loss(real_out):
    return (lambda ys: sum(map(lambda pair: max(0,1-pair[0]*pair[1])**2,  zip(ys,real_out))))
def cross_entropy_loss(real_out):
    return (lambda ys: sum(map(lambda pair: -pair[1] * math.log(pair[0]), zip(ys, real_out))))

