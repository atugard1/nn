import numpy as np

def derivative(f,x, name="", h=0.00000000000001):
    return (1/h)*(f(x+h)-f(x))

def fragment_derivative(f, xs, i, j=-1):
    """
    f  (1st arg): function of type R^m -> R^n
    xs (2nd arg): an array [x_1, ..., x_m]
    i  (3rd arg): fragment of derivative to take.
    j (4rth arg): optional. which entry to return.
    returns  [(df_1/dx_i)(xs), (df_2/dx_i)(xs), ..., (df_n/dx_i)(xs)][j],
             where f_j(xs) = f(xs)[j] = [f_1(xs), ..., f_n(xs)][j]
    """
    xs1 = list(xs[0:i])
    x   = xs[i]
    xs2 = list(xs[i+1:len(xs)])
    #actually computes not the partial but the whole vector (df_1/dx_i, df_2/dx_i, ..., df_n/dx_i)
    if 0 <= j < len(xs):
        return derivative(lambda x_: f(np.array(xs1 + [x_] + xs2)), x)[j]
    else:
        return derivative(lambda x_: f(np.array(xs1 + [x_] + xs2)), x)

def total_derivative(f, xs):
    """
    f  (1st arg): function of type R^m -> R^n
    xs (2nd arg): an array [x_1, ..., x_m]    
    returns the matrix whose ijth entry is df_i/dx_j
    """
    result = []
    for i in range(len(xs)):
        result += [fragment_derivative(f, xs, i)]
    return np.array(result).T #before transpose, holds [[df_1/dx_1, ..., df_n/dx_1], ..., [df_1/dx_m, ..., df_n/dx_m]]
                              #so after transpose,     [[df_1/dx_1, ..., df_1/dx_m], ..., [df_n/dx_1, ..., df_n/dx_m,]], which is the jacobian! 
