\documentclass[8pt]{article}

\usepackage[
backend=biber,
style=alphabetic,
sorting=ynt
]{biblatex}
\addbibresource{thesis.bib}

\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsfonts, amsthm,amssymb,graphicx,mathtools,tikz, tikz-cd, hyperref, enumitem}

\usetikzlibrary{positioning}
\usepackage{tikz}
\usepackage{tikz,fullpage}
\usetikzlibrary{arrows,%
                petri,%
                topaths}%
\usepackage{tkz-berge}

\newlist{todolist}{itemize}{2}
\setlist[todolist]{label=$\square$}
\usepackage{pifont}
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%
\newcommand{\done}{\rlap{$\square$}{\raisebox{2pt}{\large\hspace{1pt}\cmark}}%
\hspace{-2.5pt}}
\newcommand{\wontfix}{\rlap{$\square$}{\large\hspace{1pt}\xmark}}

\newcommand{\pd}[2]{\cfrac{\partial #1}{\partial #2}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathcal{Q}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Prob}{\mathbb{P}}
\newcommand{\q}{\mathbb{Q}}
\newcommand{\cx}{\mathbb{C}}
\newcommand{\real}{\mathcal{R}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\io}{\iota}
\newcommand{\iin}{\iota}
\newcommand{\obs}{\omega}
\newcommand{\iout}{\omega}
\newcommand{\im}{\text{im}}
\newcommand{\field}{\mathbb{F}}
\newcommand{\Dist}{\text{Dist}}
\newcommand{\ita}[1]{\textit{#1}}
\newcommand{\com}[2]{#1\backslash#2}
\newcommand{\oneton}{\{1,2,3,...,n\}}
\newcommand{\ket}[1]{|#1\rangle}

\newcommand{\bra}[1]{\langle#1|}
\newcommand{\bracket}[2]{\bra{#1}\ket{#2}}
\newcommand{\abs}[1]{|#1|}
\newcommand{\card}[1]{|#1|}
\renewcommand{\>}{\rangle}
\newcommand{\<}{\langle}
\newcommand\idea[1]{\begin{gather*}#1\end{gather*}}
\newcommand{\argmax}[1]{\operatornamewithlimits{argmax}\limits_{#1}}
\newcommand{\argmin}[1]{\operatornamewithlimits{argmin}\limits_{#1}}
\newcommand\ef{\ita{f} }
\newcommand\eff{\ita{f}}
\newcommand\proofs[1]{\begin{proof}#1\end{proof}}
\newcommand\inv[1]{#1^{-1}}
\newcommand\set[1]{\{#1\}}
\newcommand\en{\ita{n }}
\newcommand{\vbrack}[1]{\langle #1\realangle}
\newcommand{\norm}[1]{\lVert #1 \rVert}
\newcommand{\res}[2]{#1\bigg\rvert_{#2}}
\newcommand{\spn}[0]{\text{span}}
\newtheorem{thm}{Theorem}[section]
\newtheorem{defn}{Definition}[thm]
\newtheorem*{defn*}{Definition}
\newtheorem*{remark}{Remark}
\newtheorem{prop}{Proposition}
\newtheorem*{prop*}{Proposition}
\newtheorem{ex}{Example}[thm]
\newtheorem{lemma}{Lemma}[thm]
\newtheorem{exercise}{Exercise}[thm]
\newcommand{\qs}[1]{|#1\rangle}
\newcommand{\aqs}[1]{\langle #1|}
\newcommand{\iqs}[3]{\aqs{#1} #2 \qs{#3}}
\newcommand{\inner}[2]{\langle #1 | #2 \rangle}
\usepackage{tikz-cd}


\hypersetup{
  colorlinks,
  linkcolor=blue
}


\begin{document}


\section{Deep Nets}
\subsection{Convolutions}
For $x \in \mathbb{R}^n$ we have 
\begin{align*}
  (f \star g)(x) := \int_{\mathbb{R}^n} f(y)g(x-y) dy
\end{align*}
We check explicitly that this is commutative
\begin{align*}
  (f \star g)(x)  
  &=
    \int_{-\infty}^{\infty} \cdots \int_{-\infty}^{\infty} f(y_1, \dots, y_n)g(x_1- y_1, \dots, x_n- y_n) dy_1 \dots dy_n \\
  &=
    \int_{\infty}^{-\infty} \cdots \int_{\infty}^{-\infty} f(x_1-u_1, \dots, x_n - u_n)g(u_1, \dots, u_n) |\cfrac{\partial(y_1, \dots, y_n)}{\partial (u_1, \dots, u_n)}| du_1 \dots du_n    \\
  &=
    \int_{\infty}^{-\infty} \cdots \int_{\infty}^{-\infty} f(x_1 -u_1, \dots, x_n - u_n)g(u_1, \dots, u_n) (-1)^nI du_1 \dots du_n        \\
  &=
    \int_{-\infty}^{\infty} \cdots \int_{-\infty}^{\infty} f(x_1-u_1, \dots, x_n - u_n)g(u_1, \dots, u_n) du_1 \dots du_n        \\    
  &=
    (g \star f)(x)
\end{align*}
We check linearity
\begin{align*}
  (f \star (\alpha g + \beta h)) (x)
  &=
    \int_{\mathbb{R}^n} f(y) (\alpha g + \beta h)(x - y)  dy \\
  &=
    \int_{\mathbb{R}^n} f(y) (\alpha g(x - y) + \beta h(x - y)) dy \\
  &=
    \alpha \int_{\mathbb{R}^n} f(y)g(x - y) dy +     \beta \int_{\mathbb{R}^n} f(y)h(x - y) dy\\
  &=   \alpha (f \star g ) (t) +   \beta (f \star h) (t),
\end{align*}
and through commutativity we get bilinearity.

We define the discrete version as
\begin{align*}
  (f \star g)(x) := \lim\limits_{N \to \infty} \sum_{y_1, \dots, y_n = -N}^{N} f(y_1, \dots, y_n)g(x_1 - y_1, \dots, x_n - y_n)
\end{align*}
We check commutativity:
\begin{align*}
  (f \star g)(x_1, \dots x_n)
  &=
    \lim\limits_{N \to \infty} \sum_{y_1, \dots, y_n=-N}^{N} f(y_1, \dots y_n)g(x_1- y_1, \dots, x_n - y_n) \\
  &=
    \lim\limits_{N \to \infty} \sum_{u_1 = x_1 - N, \dots, u_n =x_n-N}^{x_1+N, \dots, x_n + N} f(x_1-u_1, \dots x_n - u_n)g(u_1, \dots, u_n)   \\
  &=
    \lim\limits_{N \to \infty} \sum_{u_1, \dots, u_n=-N}^{N} f(x_1-u_1, \dots, x_n - u_n)g(u_1, \dots, u_n)       \\
  &=
    (g \star f) (x_1, \dots, x_n),
\end{align*}
and linearity
\begin{align*}
  (f \star (\alpha g + \beta h)) (x)
  &=
    \lim\limits_{n \to \infty} \sum_{y_1, \dots, y_n=-N}^{N} f(y)((\alpha g + \beta h)(x- y)) \\
  &=
    \lim\limits_{n \to \infty} \sum_{y_1, \dots, y_n=-N}^{N} f(y)(\alpha g(x-y) + \beta h(x - y))\\
  &=
    \alpha \lim\limits_{n \to \infty} \sum_{y_1, \dots, y_n=-N}^{N} f(y)g(x-y)
    + \beta \lim\limits_{n \to \infty} \sum_{y_1, \dots, y_n=-N}^{N} f(y)h(x-y)) \\
  &=
    \alpha (f \star g)(x) + \beta (f \star h)(x),
\end{align*}
and again through commutativity we get bilinearity.

We define for a $m \times n$ dimensional matrix $A = (a_{ij})$ the discrete valued function
\begin{align*}
  f_A(i,j)
  =
  \begin{cases}
    a_{ij} & \text{ if } 1 \leq i \leq m, 1 \leq j \leq m \\
    0 & \text{ otherwise }
  \end{cases},
\end{align*}
and for matrices $A,B$
\begin{align*}
  (A \star B)_{ij} =   ((f_A \star f_B)(i+1,j+1))_{ij}.
\end{align*}


Using the indicator function $\mathbbm{1}_A(i,j)$ that is 1 if $i,j$ are within the row and column bounds of the matrix $A$ and is zero otherwise we find 
\begin{align*}
  f_{\alpha A + \beta B}(i,j) = \alpha a_{ij}\mathbbm{1}_A(i,j) + \beta b_{ij} \mathbbm{1}_{B}(i,j) = \alpha f_{A}(i,j) + \beta f_B(i,j),
\end{align*}
and so
\begin{align*}
  (W \star (\alpha A + \beta B))_{ij}
  &=
    (f_W \star f_{\alpha A + \beta B}) (i+1,j+1) \\
  &=
    f_W \star ( \alpha f_{A} (i+1,j+1) + \beta f_{B}(i+1,j+1)) \\
  &=
    \alpha f_W(i+1,j+1) \star f_{A} (i+1,j+1) + \beta f_W(i+1,j+1) \star f_{B}(i+1,j+1)) \\
  &=
    \alpha (W \star A)_{ij} + \beta (W \star B)_{ij}.
\end{align*}
We have commutativity as well:
\begin{align*}
  (A \star B)_{ij} = f_A \star f_B(i+1,j+1) = f_B \star f_A (i+1, j+1) = (B \star A)_{ij}.
\end{align*}
From these two pieces of information we get that the matrix convolution is bilinear.


\begin{ex}
  Let
  \begin{align*}
    W
    =
    \begin{pmatrix}
      1 & 2 & 3 \\
      4 & 5 & 6 \\
      7 & 8 & 9
    \end{pmatrix} \quad
    F = 
    \begin{pmatrix}
      4 & 5 \\
      6 & 7
    \end{pmatrix},
  \end{align*}
  We explicitly compute
  \begin{align*}
    (f_W \star f_F)(2,2) 
    &=
      f_W(1,1)f_F(1,1) \\
    &=
      4, \\
    (f_W \star f_F)(2,3)
    &=
      f_W(1,1)f_F(1,2) + f_W(1,2)f_F(1,1) \\
    &=
      1 \cdot 5 +  2 \cdot 1 \\
    &=
      7, \\
    f_W \star f_F(2,4)
    &=
      f_W(1,2)f_F(1,2) + f_W(1,3)f_F(1,1)  \\
    &=
      2 \cdot 5  + 3 \cdot 1 \\
    &=
      13, \\
    f_W \star f_F(2,5)
    &=
      f_W(1,3)f_F(1,2) = 3 \cdot 5 \\
    &=
      5, \\
    f_W \star f_F(3,2)
    &=
      f_W(1,1)f_F(2,1) + f_W(2,1)f_F(1,1) \\
    &=
      1 \cdot 6 + 4 \cdot 4 \\
    &=
      24, \\
    f_W \star f_F (3,3)
    &=
      f_W(1,1) f_F(2,2) + f_W(1,2)f_F(2,1) + f_W(2,1)f_F(1,2) + f_W(2,2) f_F(1,1) \\
    &=
      1 \cdot 7 + 2 \cdot 6 + 4 \cdot 5  + 5 \cdot 4 \\
    &=
      59, \\
    f_W \star f_F (3,4)
    &=
      f_W(1,2) f_F(2,2) + f_W(1,3)f_F(2,1)  + f_W(2,2) f_F(1,2) + f_W(2,3) f_F(1,1) \\
    &=
      2 \cdot 7 + 3 \cdot 6 + 5 \cdot  5 + 6 \cdot 4 \\
    &=
      81, \\
    f_W \star f_F (3,5)
    &=
      f_W(1,3) f_F(2,2) + f_W(2,3) f_F(1,2) \\
    &=
      3 \cdot 7 + 6 \cdot 5 \\
    &=
      51, \\
    f_W \star f_F(4,2)
    &=
      f_W(2,1) f_F(2,1) + f_W(3,1)f_F(1,1) \\
    &=
      5 \cdot 6 + 7 \cdot 4 \\
    &=
      58, \\
    f_W \star f_F(4,3)
    &=
      f_W(2,1) f_F(2,2) + f_W(2,2) f_F(2,1) f_W(3,1) f_F(1,2) + f_W(3,2)f_F(1,1) \\
    &=
      4 \cdot 7 + 5 \cdot 6 + 7 \cdot 5 + 8 \cdot 4 \\
    &=
      125, \\
    f_W \star f_F (4,4)
    &=
      f_W(2,2) f_F(2,2) + f_W(2,3) f_F(2,1) + f_W(3,2) f_F(1,2) + f_W(3,3)f_F(1,1) \\
    &=
      5 \cdot 7 + 6 \cdot 6 + 8 \cdot 5 + 9 \cdot 4 \\
    &=
      147, \\
    f_W \star f_F (4,5)
    &=
      f_W(2,3)f_F(2,2) + f_W(3,3)f_F(1,2) \\
    &=
      6 \cdot 7 + 9 \cdot 5 \\
    &=
      87, \\
    f_W \star f_F (5,2)
    &=
      f_W(3,1) f_F(2,1) \\
    &=
      42, \\
    f_W \star f_F(5,3)
    &=
      f_W(3,1) f_F(2,2) + f_W(3,2) f_F(2,1) \\
    &=
      7 \cdot 7 + 8 \cdot 6 \\
    &=
      97, \\
    f_W \star f_F(5,4) \\
    &=
      f_W(3,2) f_F(2,2) + f_W(3,3)f_F(2,1) \\
    &=
      8 \cdot 7 + 9 \cdot 6 \\
    &=
      110, \\
    f_W \star f_F(5,5)
    &=
      f_W(3,3) f_F(2,2) \\
    &=
      63,
  \end{align*}
  and for all other integers $i,j$ $f_W \star f_F(i,j) = 0$. We have
  \begin{align*}
    W \star F =
    \begin{pmatrix}
      4 & 7 & 13 & 5 \\
      24 & 59 & 81 & 51 \\
      58 & 125 &  147 & 87 \\
      42 & 97 & 110 & 63
    \end{pmatrix}.
  \end{align*}
\end{ex}
We see from the previous example that this matrix convolution definition corresponds to a fully padded convolution operation with stride 1 in neural network terminology.

We could just as well have defined n-tensor convolution, for example for $n=3$ like
\begin{align*}
  (A \star B)_{ijk} = \sum_{s,t,k} f_A(s,t,u) f_B(i+1-s,j+1-t,k+1-u).
\end{align*}
It's clear that this is linear and commutative.

\subsection{Transformers}
\subsubsection{Positional Encoding}
We have

\begin{align*}
  p_{ij} =
  \begin{cases}
    \sin(\xi(i,j)) & \text{if } i \mod 2 \equiv 0 \\
    \cos(\xi(i-1,j)) & \text{otherwise}
  \end{cases}
\end{align*}
for
\begin{align*}
  \xi(i,j) = \cfrac{j}{N^{i/d_m}},
\end{align*}
for N a free parameter, and $d_m$ the dimension of the model. Using this we construct the positional encoding matrix
\begin{align*}
  p = (p_{ij}),
\end{align*}
for $0 \leq i < d_m$, $0 \leq j < b$, and $b$ the batch size.
\subsubsection{Self Attention}
Suppose we have a finite collection of symbols $\mathcal{S}$. We order the symbols as $S = (s_1, s_2, \dots, s_{|\mathcal{S}|})$, and map the $i$th symbol to the $i$th standard basis vector $e_i \in \mathbb{R}^{n}$; this is the vector whose entries are all 0 except at the $i$th row, where there's a 1:
\begin{align*}
  e_i =
  \begin{pmatrix}
    0 \\
    \vdots \\
     1 \\
    \vdots \\
    0
  \end{pmatrix}.
\end{align*}
Let $[s_i] = e_i$. We extend this to arbitrary sequences of symbols $\sigma_1 \sigma_2 \dots \sigma_n$ for $\sigma_i \in \mathcal{S}$ by
\begin{align*}
  [\sigma_1 \dots \sigma_n] =
  \begin{pmatrix}
    [\sigma_1] & \hdots & [\sigma_n])
  \end{pmatrix}
\end{align*}
Let $E \in \mathbb{R}^{d_m \times |\mathcal{S}|}$ be a matrix. We encode our sequences of symbols as 
\begin{align*}
  E[\sigma_1 \dots \sigma_n]
  =
  E
  \begin{pmatrix}
    [\sigma_1] & \hdots & [\sigma_n])
  \end{pmatrix}
  =
  \begin{pmatrix}
    E[\sigma_1] & \hdots & E[\sigma_n])
  \end{pmatrix}.
\end{align*}

Let $x_i = E[\sigma_i]$, and
\begin{align*}
  X
  =
  \begin{pmatrix}
    x_1 & \hdots & x_n
  \end{pmatrix}
\end{align*}

We produce queries $q_i$, keys $k_i$, and values $v_i$, using matrices 
$W_q \in \mathbb{R}^{d_q \times d_m}$, $W_k \in \mathbb{R}^{d_k \times d_m}$, and $W_v \in \mathbb{R}^{d_v \times d_m}$ according to 
\begin{align*}
  q_i &= W_q x_i, \\
  k_i &= W_k x_i, \\
  v_i &= W_v x_i.
\end{align*}
We define matrices $K \in \mathbb{R}^{d_k \times n}$, $Q \in \mathbb{R}^{d_q \times n }$,  and $V \in \mathbb{R}^{d_v \times n }$:
\begin{align*}
  K
  =
  \begin{pmatrix}
    k_1 & \hdots & k_n
  \end{pmatrix}
  =
  \begin{pmatrix}
    W_kx_1 & \hdots & W_k x_n
  \end{pmatrix}
  =
  W_k
  \begin{pmatrix}
    x_1 & \hdots & x_n
  \end{pmatrix}
  =
  W_kX,
\end{align*}
\begin{align*}
  Q
  =
  \begin{pmatrix}
    q_1 & \hdots & q_n
  \end{pmatrix}
  =
  \begin{pmatrix}
    W_qx_1 & \hdots & W_q x_n
  \end{pmatrix}
  =
  W_q
  \begin{pmatrix}
    x_1 & \hdots & x_n
  \end{pmatrix}
  =
  W_qX,
\end{align*}
and
\begin{align*}
  V
  =
  \begin{pmatrix}
    v_1 & \hdots & v_n
  \end{pmatrix}
  =
  \begin{pmatrix}
    W_vx_1 & \hdots & W_v x_n
  \end{pmatrix}
  =
  W_v
  \begin{pmatrix}
    x_1 & \hdots & x_n
  \end{pmatrix}
  =
  W_vX.
\end{align*}

For what follows to make sense we require that $d_q = d_k$. In that case
\begin{align*}
  K^TQ =
  \begin{pmatrix}
    k_1^T \\
    \vdots \\
    k_n^T
  \end{pmatrix}
  \begin{pmatrix}
    q_1 & \hdots & q_n
  \end{pmatrix}
  =
  \begin{pmatrix}
    k_1 \cdot q_1 & \hdots & k_1 \cdot q_n \\
         \vdots   & \ddots & \vdots\\
    k_n \cdot q_1 & \hdots & k_n \cdot q_n
  \end{pmatrix}.
\end{align*}

We rescale this by $\sqrt{d_k}$, and take the soft max to get

\begin{align*}
  \text{softmax}(\cfrac{1}{\sqrt{d_k}} K^TQ) =
  \begin{pmatrix}
    \cfrac{\exp(\cfrac{1}{\sqrt{d_k}}k_1 \cdot q_1)}{\sum\limits_j \exp(\cfrac{1}{\sqrt{d_k}}k_j \cdot q_1)} & \hdots & \cfrac{\exp(\cfrac{1}{\sqrt{d_k}}k_1 \cdot q_n)}{\sum\limits_j \exp(\cfrac{1}{\sqrt{d_k}}k_j \cdot q_n)} \\
         \vdots   & \ddots & \vdots\\
    \cfrac{\exp(\cfrac{1}{\sqrt{d_k}}k_n \cdot q_1)}{\sum\limits_j \exp(\cfrac{1}{\sqrt{d_k}}k_j \cdot q_1)} & \hdots & \cfrac{\exp(\cfrac{1}{\sqrt{d_k}}k_n \cdot q_n)}{\sum\limits_j \exp(\cfrac{1}{\sqrt{d_k}}k_j \cdot q_n)}.
  \end{pmatrix}
\end{align*}
Let
\begin{align*}
  \alpha_{ij} = \cfrac{\exp(\cfrac{1}{\sqrt{d_k}}k_i \cdot q_j)}{\sum\limits_l \exp(\cfrac{1}{\sqrt{d_k}}q_j \cdot k_l)}.
\end{align*}
We have

\begin{align*}
  \sum\limits_{j} \alpha_{ji} v_j  =
  \sum\limits_{j} \alpha_{ji}  
  \begin{pmatrix}
    v_{1j} \\
    \vdots \\
    v_{d_v j}
  \end{pmatrix} =
  \begin{pmatrix}
    \sum\limits_{j} v_{1j} \alpha_{ji}  \\
    \vdots \\
    \sum\limits_{j} v_{d_vj} \alpha_{ji}  \\    
  \end{pmatrix},  
\end{align*}
so that
\begin{align*}
  V (\text{softmax}(\cfrac{1}{\sqrt{d_k}} K^TQ))
  &=
    \begin{pmatrix}
      v_1 & \hdots & v_n
    \end{pmatrix}    
    \begin{pmatrix}
      \alpha_{11} & \hdots & \alpha_{1n} \\
      \vdots   & \ddots & \vdots\\
      \alpha_{n1} & \hdots & \alpha_{nn}.      
    \end{pmatrix} \\
  &=
    \begin{pmatrix}
      v_{11} & \hdots & v_{1n} \\
      \vdots   & \ddots & \vdots\\
      v_{d_v1} & \hdots & v_{d_vn}     
    \end{pmatrix}    
    \begin{pmatrix}
      \alpha_{11} & \hdots & \alpha_{1n} \\
      \vdots   & \ddots & \vdots\\
      \alpha_{n1} & \hdots & \alpha_{nn}.      
    \end{pmatrix} \\    
  &=
    \begin{pmatrix}
      \sum\limits_{j} v_{1j}\alpha_{j1} & \hdots & \sum\limits_{j} v_{1j}\alpha_{jn} \\
      \vdots   & \ddots & \vdots\\      
      \sum\limits_{j} v_{d_vj}\alpha_{j1} & \hdots & \sum\limits_{j} v_{d_vj}\alpha_{jn}
    \end{pmatrix} \\
  &=
    \begin{pmatrix}
      \sum\limits_{j} \alpha_{j1} v_j  & \hdots & \sum\limits_{j} \alpha_{jn} v_j
    \end{pmatrix}.
\end{align*}
We collect the matrices into a list $H = (W_q, W_k, W_v)$, and call this a head. Using currying, we define attention directly on the head as 
\begin{align*}
  \Lambda(H)(X) =V(\text{softmax}(\cfrac{1}{\sqrt{d_k}} K^TQ) =  W_vX(\text{softmax}(\cfrac{1}{\sqrt{d_k}} W_kXX^TW_q^T))
\end{align*}
\subsubsection{Future masking}
We interpret the $j$-th index in $\alpha_{ij}$ as the given word at time $j$, and the $i$ index as the probability of the word at time $i$, where queries and keys are now word embeddings. In that case, all entries below the diagonal are in the future. We mask these words by adding a lower triangular matrix $M \in \mathbb{R}^{n \times n}$ to $\cfrac{1}{\sqrt{d_k}}K^TQ$ with high negative values below the diagonal and zeros everywhere else
\begin{align*}
  \Lambda_M(H)(X) =V (\text{softmax}(M + \cfrac{1}{\sqrt{d_k}} K^TQ)) =  W_vX(\text{softmax}(M + \cfrac{1}{\sqrt{d_k}} X^TW_k^TW_q X))
\end{align*}

\subsubsection{Multi-head Attention}

Suppose now that we have a sequence of heads $H = (H_1, \dots, H_m)$, where $H_i = (W_q^i, W_k^i, W_v^i)$. We define the following block matrix

\begin{align*}
  B(H,X) =
  \begin{pmatrix}
    \Lambda( H^1)(X) \\
    \vdots \\
    \Lambda(H^m)(X)
  \end{pmatrix}
\end{align*}
We need one more ingredient to define multi-head attention $\tilde \Lambda$, a matrix $W_o$ such that
\begin{align*}
  \tilde \Lambda(H, W_o)(X) = W_o B(H,X).
\end{align*}
Since $\Lambda(H^i)(X)$ is $d_v^i \times n$ dimensional, we have that $B(H,X)$ is $ (\sum\limits_{i=1}^m d_v^i) \times n$ dimensional, which implies that $W_o$ must be
($d_o \times \sum\limits_{i=1}^m d_v^i)$ dimensional.

\subsection{Backpropagation}

Suppose we have a neural net with layer indices $0,1,2, \dots, n$, where the first layer $\ell = 0$ is the input layer and the last layer $\ell=n$ is the output layer. We let $m_\ell$ denote the dimension of the $\ell$-th layer. We write $W^\ell$ for the weight matrix going from layer $\ell$ to layer $\ell+1$.
We write 
\begin{align*}
  \sigma_\ell(x_\ell) =
  \begin{pmatrix}
    \sigma_\ell^1(x_\ell) \\
    \vdots \\
    \sigma_\ell^{m_\ell}(x_\ell)
  \end{pmatrix},
\end{align*}
for the activation function of the $\ell_{th}$ layer. We merely assume that $\sigma_\ell$ are all differentiable, and make no further assumptions. Let $L$ be the loss function of this network.

We define $\delta(x_\ell^i, L) = \cfrac{\partial L}{\partial x_\ell^i}$ for $x_\ell^i \in \mathbb{R}$, and for $x_\ell \in \mathbb{R}^n$
\begin{align*}
  \delta(x_\ell, L) =
  \begin{pmatrix}
    \cfrac{\partial L}{\partial x_\ell^1}, \\
    \vdots \\
    \cfrac{\partial L}{\partial x_\ell^n}, \\ 
  \end{pmatrix}.
\end{align*}
We adopt the convention that $x_\ell$ is the pre-activation vector and that $y_{\ell} = \sigma_{\ell}(x_\ell)$ is the post-activation vector of the $\ell$-th layer. 

We have
\begin{align*}
  D_{x_n}y_n =
  \begin{pmatrix}
    \cfrac{\partial y_n^1}{\partial x_n^1} & \cdots & \cfrac{\partial y_n^1}{\partial x_n^{m_n}} \\
    \cfrac{\partial y_n^2}{\partial x_n^1} & \cdots & \cfrac{\partial y_n^2}{\partial x_n^{m_n}} \\
    \vdots        & \ddots &             \vdots            \\
    \cfrac{\partial y_n^{m_n}}{\partial x_n^1} & \cdots & \cfrac{\partial y_n^{m_n}}{\partial x_n^{m_n}}
  \end{pmatrix}.
\end{align*}
We multiply $\delta(y_n,L)$ by $(D_{x_n}y_n)^T$, and get
\begin{align*}
  (D_{x_n}y_n)^T \delta(y_n,L)
  = 
  \begin{pmatrix}
    \sum\limits_k \cfrac{\partial L}{\partial y_n^k} \cfrac{\partial y_n^k}{\partial x_n^1}  \\
    \vdots \\
    \sum\limits_k \cfrac{\partial L}{\partial y_n^k} \cfrac{\partial y_n^k}{\partial x_n^{m_n}}  \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \cfrac{\partial L}{\partial x_n^1}  \\
    \vdots \\
    \cfrac{\partial L}{\partial x_n^{m_n}} \\
  \end{pmatrix} 
  =
  \begin{pmatrix}
    \delta(x_n^1, L)  \\
    \vdots \\
    \delta(x_n^{m_n}, L)    
  \end{pmatrix} 
  = \delta(x_n, L)
\end{align*}
Next, we multiply on the left by $W^{n-1}$ and get:
\begin{align*}
  W^{n-1} \delta(x_n, L)= 
  \begin{pmatrix}
    \sum\limits_k w_{1k} \cfrac{\partial L}{\partial x_n^k}  \\
    \sum\limits_k w_{2k} \cfrac{\partial L}{\partial x_n^k}  \\
    \vdots \\
    \sum\limits_k w_{m_{n-1} k} \cfrac{\partial L}{\partial x_n^k}  \\        
  \end{pmatrix}.
\end{align*}
We take the derivative of the post activation vector with respect to the pre activation vector of the previous layer, arriving at
\begin{align*}
  D_{x_{n-1}}y_{n-1} =
  \begin{pmatrix}
    \cfrac{\partial y_{n-1}^1}{\partial x_{n-1}^1} & \cdots & \cfrac{\partial y_{n-1}^1}{\partial x_{n-1}^{m_{n-1}}} \\
    \cfrac{\partial y_{n-1}^2}{\partial x_{n-1}^1} & \cdots & \cfrac{\partial y_{n-1}^2}{\partial x_{n-1}^{m_{n-1}}} \\
    \vdots        & \ddots &             \vdots            \\
    \cfrac{\partial y_{n-1}^{m_{n-1}}}{\partial x_{n-1}^1} & \cdots & \cfrac{\partial y_{n-1}^{m_{n-1}}}{\partial x_{n-1}^{m_{n-1}}}
  \end{pmatrix}. 
\end{align*}
Multiplying (put eqn number) by the transpose of (put eqn number), we arrive at
\begin{align*}
  (D_{x_{n-1}}y_{n-1})^T W^{n-1} \delta(x_n, L) 
  &=  
  \begin{pmatrix}
    \cfrac{\partial y_{n-1}^1}{\partial x_{n-1}^1} & \cdots & \cfrac{\partial y_{n-1}^{m_{n-1}}}{\partial x_{n-1}^1} \\
    \cfrac{\partial y_{n-1}^1}{\partial x_{n-1}^2} & \cdots & \cfrac{\partial y_{n-1}^{m_{n-1}}}{\partial x_{n-1}^{2}} \\
    \vdots        & \ddots &             \vdots            \\
    \cfrac{\partial y_{n-1}^1}{\partial x_{n-1}^{m_{n-1}}} & \cdots & \cfrac{\partial y_{n-1}^{m_{n-1}}}{\partial x_{n-1}^{m_{n-1}}}
  \end{pmatrix}
  \begin{pmatrix}
    \sum\limits_k w_{1k} \cfrac{\partial L}{\partial x_n^k}  \\
    \sum\limits_k w_{2k} \cfrac{\partial L}{\partial x_n^k}  \\
    \vdots \\
    \sum\limits_k w_{m_{n-1} k} \cfrac{\partial L}{\partial x_n^k}  \\        
  \end{pmatrix}  \\
  &=
  \begin{pmatrix}
    \sum\limits_{k}  (\sum\limits_{j} w_{jk}\cfrac{\partial y_{n-1}^j}{\partial x_{n-1}^1})  \cfrac{\partial L}{\partial x_n^k}  \\
    \sum\limits_{k}  (\sum\limits_{j} w_{jk} \cfrac{\partial y_{n-1}^j}{\partial x_{n-1}^2})  \cfrac{\partial L}{\partial x_n^k}  \\
    \vdots \\
    \sum\limits_{k}  (\sum\limits_{j} w_{jk} \cfrac{\partial y_{n-1}^j}{\partial x_{n-1}^{m_{n-1}}})  \cfrac{\partial L}{\partial x_n^k}  \\
  \end{pmatrix} \\
  &=
  \begin{pmatrix}
    \sum\limits_{k}  \cfrac{\partial}{\partial x_{n-1}^1}(\sum\limits_{j} w_{jk} y_{n-1}^j)  \cfrac{\partial L}{\partial x_n^k}  \\
    \sum\limits_{k}  \cfrac{\partial}{\partial x_{n-1}^2}(\sum\limits_{j} w_{jk} y_{n-1}^j)  \cfrac{\partial L}{\partial x_n^k}  \\
    \vdots \\
    \sum\limits_{k}  \cfrac{\partial}{\partial x_{n-1}^{m_{n-1}}}(\sum\limits_{j} w_{jk} y_{n-1}^j)  \cfrac{\partial L}{\partial x_n^k}  \\
  \end{pmatrix} \\
  &=
  \begin{pmatrix}
    \sum\limits_{k}  \cfrac{\partial x_n^k}{\partial x_{n-1}^1} \cfrac{\partial L}{\partial x_n^k}  \\
    \sum\limits_{k}  \cfrac{\partial x_n^k}{\partial x_{n-1}^2} \cfrac{\partial L}{\partial x_n^k}  \\
    \vdots \\
    \sum\limits_{k}  \cfrac{\partial x_n^k}{\partial x_{n-1}^{m_{n-1}}} \cfrac{\partial L}{\partial x_n^k}  \\
  \end{pmatrix} \\
  &=
  \begin{pmatrix}
    \delta(x_{n-1}^1, L) \\
    \vdots \\
    \delta(x_{n-1}^{m_{n-1}}, L)    
  \end{pmatrix},
\end{align*}
so that
\begin{align*}
    \delta(x_{n-1},L) = (D_{x_{n-1}}y_{n-1})^T W^{n-1} \delta(x_n, L).
\end{align*}
Since there was nothing special about $n$ in this computation, we conclude that
\begin{prop*}
  For $ 1 \leq \ell \leq n$,
  $$\delta(x_{\ell-1},L) = (D_{x_{\ell-1}}y_{\ell-1})^T W^{\ell-1} \delta(x_\ell, L).$$
\end{prop*}

Now, since
\begin{align*}
  x_{\ell+1}^k =     \sum\limits_j w_{jk} y_\ell^j    
\end{align*}
we get
\begin{align*}
  \cfrac{\partial{x_{\ell+1}^k}}{\partial w_{ij}^\ell} =  \delta_{jk} y_\ell^i,
\end{align*}
for $\delta_{jk}$ the Kronecker delta function, and 
\begin{align*}
  \cfrac{\partial L}{\partial w_{ij}^\ell}
  &=
  \sum\limits_k \cfrac{\partial L}{\partial{x_{\ell+1}^k}}\cfrac{\partial{x_{\ell+1}^k}}{\partial w_{ij}^\ell}\\
  &=
  \sum\limits_k \cfrac{\partial L}{\partial{x_{\ell+1}^k}} \delta_{jk} y_\ell^i \\
  &=
  \cfrac{\partial L}{\partial{x_{\ell+1}^j}} y_\ell^i \\
  &=
  y_\ell^i \delta(x_{\ell+1}^j,L). 
\end{align*}


Using gradient descent, therefore, the update rule is given by
\begin{align*}
  w_{ij}^\ell \leftarrow w_{ij}^\ell(1 + \lambda) - \alpha y_\ell^i \delta(x_{\ell+1}^j,L)
\end{align*}

We curry our loss function, and define a new loss function by
\begin{align*}
  \widetilde L(a)(y) =  \sum\limits_{i=1}^n L(a_i)(y_i),
\end{align*}
for $a$ the matrix whose $i$th row gives us the $i$th training output vector, and $y$ the matrix whose $i$th row gives us the $i$th output of our neural net.
We have then
\begin{align*}
  \cfrac{\partial \widetilde L(b)}{\partial w_{jk}^\ell}(y) = \sum\limits_{i=1}^n \pd{L(b_i)}{w_{jk}^\ell}(y_i) = \sum\limits_{i=1}^n y_\ell^{ij} \delta(x_{\ell+1}^{ik},L(b_i)). 
\end{align*}
The update rule in this case is given by
\begin{align*}
  w_{jk}^\ell \leftarrow w_{jk}^\ell - \alpha\sum\limits_{i=1}^n y_\ell^{ij} \delta(x_{\ell+1}^{ik},L(b_i)),
\end{align*}
and is named ``mini batch stochastic gradient descent.''


We write $\widetilde L(\lambda,a) = \sum\limits_{i=1}^n L(a_i)(y_i)  + \lambda \cdot f(w)$, where $L$ is the loss function from the previous section. By the linearity of the derivative, we simply get

\begin{align*}
  \pd{\widetilde L(\lambda)}{w^\ell_{ij}} = \sum\limits_{i=1}^n y_\ell^{ij} \delta(x_{\ell+1}^{ik},L(b_i)) + \lambda \pd{f_{ij}}{w_{ij}^\ell}(w^\ell)
\end{align*}
and so the update rule is

\begin{align*}
  w_{ij}^\ell \leftarrow w_{ij}^\ell - \alpha(\sum\limits_{i=1}^n y_\ell^{ij} \delta(x_{\ell+1}^{ik},L(b_i)) + \lambda \pd{f_{ij}}{w_{ij}^\ell}(w^\ell))  
\end{align*}

TODO: extend this section with various optimisers and also with $n$-rank tensors rather than matrices.

\section{Hopfield Nets}
We have n units with states $s_1, s_2, \dots, s_n$ forming an undirected graph. A symmetric matrix $W = (w_{ij})$ with diagonals zero, i.e. $w_{ij} = w_{ji}$ and $w_{ii} = 0$,  encodes the weights of edge $ij$; and a bias vector $b$ the bias weights.We have the energy function
\begin{align*}
  H = -(\cfrac{1}{2}s^T W s + b \cdot s) = -(\cfrac{1}{2}\sum\limits_{i \neq j} w_{ij} s_i s_j + \sum b_is_i).
\end{align*}
We have partials
\begin{align*}
  \pd{H}{w_{ij}} = -(1-\delta_{ij})\cdot s_i s_j, \quad \pd{H}{b_i}   = -s_i
\end{align*}
and so the update rule is
\begin{align*}
  w_{ij} \leftarrow w_{ij} + \alpha(1- \delta_{ij}) s_i s_j, \quad b_i   \leftarrow b_i + \alpha s_i
\end{align*}
where $\delta_{ij}$ is the Kronecker delta.

\section{Recurrent Neural Nets}

Backpropagation Through Time (what is it in full generality)

\subsection{Long Short Term Memory}

What is this?



\end{document}